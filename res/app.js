var API_KEY = '607910f82c5152e470dceeadd65a1b15';
window['arrFav'] = [];
function saveLastSearch(results){
    if(typeof(localStorage) !== 'undefined'){
        localStorage.shows = JSON.stringify(results);
    }
}

function savePopular(results){
    if(typeof(localStorage) !== 'undefined'){
        localStorage.popular = JSON.stringify(results);
    }
}

function populate(result, target){
    var style='';
    var episodes_style;

    jQuery.each(result, function(index, value){
        var box = '';
        switch(target){
            case '#results':
                var btn_label = 'add to favourites';
                var btn_class = 'add-favourite';
                var episodes_style = 'display:none';
                break;
            case '#favourites':
                var btn_label = 'remove from favourites';
                var btn_class = 'remove-favourite';
                var episodes_style = 'display:block';

                var index = window['arrFav'].indexOf(value.id);
                if(index == -1){
                    window['arrFav'].push(value.id);
                }

                break;
            case '#popular':
                var episodes_style = 'display:none';
                var btn_label = 'add to favourites';
                var btn_class = 'add-favourite';
                style = 'display:block';
                break;
        }

        var content = jQuery(target).html();
        var img = typeof(value.poster_path) !== 'undefined' ? value.poster_path : '';
        if(!img){img = typeof(value.poster_path) !== 'undefined' ? value.backdrop_path : '';}
        if(!img){img = '';}
        else {img = '<img src="http://image.tmdb.org/t/p/w92'+img+'" />';}

        var store = JSON.stringify(value);

        box = content;
        box += '<div class="box_'+value.id+'">';
        box += '<div class="padding"><h2>' + value.name + '</h2>';
        box += '<br /> ';
        box += '<h4>Rating: ' + value.vote_average + '</h4>';
        box += '<div class="container">' + img;
        var stripOverview = value.overview.substr(0,450) + ".....";
        box += '<span>' + stripOverview + '</span>';
        box += '</div></div>';
        box += "<a style='"+episodes_style+"' data-id='"+value.id+"' class='episodes_btn' href=''>Episodes  <span class='arrow'>&#x25BC;</span></a>";
        box += "<div data-id='exp_"+value.id+"' class='expandable'><div></div></div>";
        box += "<div target-id='vid_"+value.id+"' class='trailer_btn vid_"+value.id+"'>Trailer  <span class='arrow'>&#x25BC;</span></div>";
        box += "<div trigger-id='vid_"+value.id+"' class='trailer_container vid_cont_"+value.id+"'></div>";
        box += "<button object-id='"+value.id+"' style='"+style+"'  name='add-to-favourites' class='"+btn_class+"'>"+btn_label+"</button>";
        box += "<div class='clear'></div></div>";

        jQuery(target).html(box);

        if(target == '#favourites'){
            getSeriesDetails(value.id, target);
            getMedia(value.id);
        }
        //getMedia(value.id);
    });
    favouritesUpdate();
}

function saveEpisodeHelper(id, varName, data){
    //console.log(varName);
    var add = true;
    if(typeof(localStorage) !== 'undefined'){
        if(typeof(localStorage.getItem(varName)) !== 'undefined' && localStorage.getItem(varName) !== 'undefined') {
            var temp = JSON.parse(localStorage.getItem(varName));
            jQuery.each(temp, function(i,v){
                if(typeof(v.id) !== 'undefined'){
                    if(v.id == id){
                        add = false;
                    }
                }

                if(i+1 == temp.length){
                    if(add == true){
                        temp.push({id: id, series_detail:data});
                        localStorage.setItem(varName, JSON.stringify(temp));
                    }
                }
            });
        } else {
            localStorage.setItem(varName, JSON.stringify([{id: id, series_detail:data}]));
        }
    }
}

function saveSeasonHelper(id, varName, data, season){
    var added = false;
    if(typeof(localStorage) !== 'undefined'){
        if(typeof(localStorage.getItem(varName)) !== 'undefined' && localStorage.getItem(varName) !== 'undefined') {
            var temp = JSON.parse(localStorage.getItem(varName));
            jQuery.each(temp, function(i,v){
                if(typeof(v.id) !== 'undefined'){
                    if(v.id == id){
                        temp[i]['seasons'][season] = data;
                        localStorage.setItem(varName, JSON.stringify(temp));
                        added = true;
                    }

                    if(i+1 == temp.length && !added){
                        temp.push({id:id, seasons: {}});
                        temp[temp.length-1]['seasons'][season] = data;
                        localStorage.setItem(varName, JSON.stringify(temp));
                        added = true;
                    }
                }
            });
        } else {
            console.log("INIT");
            localStorage.setItem(varName, JSON.stringify([{id: id, seasons:{}}]));
        }
    }
}

function favouritesUpdate(){
    jQuery.each(window['arrFav'], function(i,v){
        var index = window['arrFav'].indexOf(parseInt(v));
        if(index !== -1){
            jQuery('.box_'+v+' button[name=add-to-favourites]').removeClass('add-favourite').addClass('remove-favourite');
            jQuery('.box_'+v+' button[name=add-to-favourites]').html('remove from favourites');
        }
    });
}

jQuery(document).on('click', '.episodes_btn', function(e){
    e.preventDefault();
    var id = jQuery(this).attr('data-id');
    jQuery('div[data-id=exp_'+id+']').slideToggle();
});

function getShows(searchTerm){
    var searchTerm = searchTerm;
    var result;
    var url = 'http://api.themoviedb.org/3/search/tv?query='+searchTerm+'&api_key='+API_KEY;

    $.get(url, function(data){
        populateShows(data);
    });
}

function populateShows(data){
    result = data.results;
    if(result.length == 0){
        jQuery('#results').html('No results');
    } else {
        jQuery('#results').html('');
        saveLastSearch(data.results);
    }

    populate(result, '#results');
}

function getSeriesDetails(id, target){
    var url = 'http://api.themoviedb.org/3/tv/'+id+'?api_key='+API_KEY;

    if(typeof(localStorage.getItem('dataSeasons')) !== 'undefined' && localStorage.getItem('dataSeasons') !== 'undefined') {
        var sel = 'div[data-id=exp_'+id+'] > div';
        var temp = JSON.parse(localStorage.getItem('dataSeasons'));
        jQuery.each(temp, function(i,v){
            if(v.id == parseInt(id)){
                var seasons = v.seasons;
                var len = seasons.length;
                jQuery.each(seasons, function(i, v){
                    var last = false;
                    if (i == len - 1) {
                        last = true;
                    }

                    var list = '<section><p>'+seasons[i].name+'</p>';
                    list += '<ul>';
                    var len = seasons[i].episodes.length;
                    jQuery.each(seasons[i].episodes, function(ind, val){
                        var name = 'show_'+id+'_s_'+i+'_e_'+ind;
                        list += '<li><input type="checkbox" data-id="'+id+'" name="'+name+'"  '+(typeof(localStorage.ep) !== 'undefined' && JSON.parse(localStorage.ep).indexOf(name) !== -1 ? "checked" : "")+'/><span>'+(parseInt(ind) + 1)+'</span></li>';

                        if (ind == len - 1) {
                            jQuery(sel).append(list+'</ul></section>');
                        }
                    });

                });
            }
        });


    } else {
        $.get(url, function(data){
            saveEpisodeHelper(id, 'dataSeries', data);
            populateSeries(data, id, target)
        });
    }

}

function populateSeries(data, id, target){
    var episodes = data.number_of_episodes;
    var seasons = data.number_of_seasons;
    var sel = 'div[data-id=exp_'+id+'] > div';
    jQuery(sel).html('<p><b>Seasons</b>: '+seasons+'</p><p><b>Episodes</b>: '+episodes+'</p>');

    if(target == '#favourites'){
        var len = data.seasons.length;
        jQuery.each(data.seasons, function(i, v){
            var last = false;
            if (i == len - 1) {
                last = true;
            }
            getEpisodes(id, v.season_number, sel, data, last);
        });
    }
}

function getEpisodes(id, season_number, sel, series_data, last){
    var url = 'http://api.themoviedb.org/3/tv/'+id+'/season/'+season_number+'?api_key='+API_KEY;
    $.get(url, function(data){
        saveSeasonHelper(id, 'dataSeasons', data, season_number);
        populateEpisodes(data, id, season_number, sel, last);
    });
}

function populateEpisodes(data, id, season_number, sel, last){
    var list = '<section><p>'+data.name+'</p>';
    list += '<ul>';
    var len = data.episodes.length;
    jQuery.each(data.episodes, function(i, v){
        var name = 'show_'+id+'_s_'+season_number+'_e_'+v.episode_number;
        list += '<li><input type="checkbox" data-id="'+id+'" name="'+name+'"  '+(typeof(localStorage.ep) !== 'undefined' && JSON.parse(localStorage.ep).indexOf(name) !== -1 ? "checked" : "")+'/><span>'+v.episode_number+'</span></li>';

        if (i == len - 1) {
            jQuery(sel).append(list+'</ul></section>');
        }
    });
}

function getPopular(){
    var result;
    var sel = '#popular';
    var url = 'http://api.themoviedb.org/3/tv/popular?api_key='+API_KEY;

    $.ajax({
        url: url,
        data: '',
        success: function(data){
            result = data.results;
            savePopular(result);
            if(result.length == 0){
                jQuery(sel).html('No results');
            } else {
                jQuery(sel).html('');
            }

            populate(result, sel);
        },
        dataType: 'json'
    });
}

function getMedia(id){
    var result;
    var sel = '#search';
    var url = 'http://api.themoviedb.org/3/tv/'+id+'/videos?api_key='+API_KEY;

    $.ajax({
        url: url,
        data: '',
        success: function(data){
            jQuery.each(data.results, function(i,v){
                var link = 'www.youtube.com/watch?v='+ v.key;
                var html = '<iframe src="http://www.youtube.com/embed/'+v.key+'" width="100%" height="200" frameborder="0"></iframe>';
                if(v.key){
                    jQuery('.vid_cont_'+id).html(html);
                    jQuery('.vid_'+id).show();
                }
            });
        },
        erroe: function(){
            console.log("err");
        },
        dataType: 'json'
    });
}

function addFavourites(id){
    localStorage.setItem('dataSeasons', "undefined");
    var results = mergeArrays(JSON.parse(localStorage.shows), JSON.parse(localStorage.popular));
    var obj = '';
    jQuery.each(results, function(index, value){
        var arr = [];
        if(value.id == id){
            obj = value.value;
        }
    });

    if(typeof(localStorage) !== 'undefined'){
        if(typeof(localStorage.fav) !== 'undefined' && localStorage.fav !== 'undefined'){
            var store = JSON.parse(localStorage.fav) || [];
            store.push(obj);
            localStorage.fav = JSON.stringify(store);
        } else {
            localStorage.fav = JSON.stringify([obj]);
        }
    }
    var fav = JSON.parse(localStorage.fav);
    jQuery('#favourites').html('');
    populate(fav, '#favourites');

}

function removeFavourite(id){
    if(localStorage.fav !== 'undefined'){
        var fav = JSON.parse(localStorage.fav);
        var obj_id = id;
        jQuery.each(fav, function(index, value){
            if(typeof(value)  !== 'undefined'  && value.id == obj_id){
                fav.splice(index, 1);
                localStorage.fav = JSON.stringify(fav);
            }
        });
        jQuery('#favourites').html('');
        populate(fav, '#favourites');
    }
    var index = window['arrFav'].indexOf(parseInt(id));
    if(index !== -1){
        window['arrFav'].splice(index, 1);
        console.log(window['arrFav']);
    }
}

jQuery('#searchTerm').keyup(function(data){
    if(jQuery(this).val().length > 1){
        var term = jQuery(this).val();
        getShows(term);
    }
});

jQuery('ul.tabs > li > a').on('click', function(e){
    e.preventDefault();
    jQuery('ul.tabs > li').removeClass('active');
    jQuery(this).parent().addClass('active');
    jQuery('div[data-id]').hide();
    var sel = jQuery(this).parent().attr('data-id');
    jQuery('div[data-id='+sel+']').show();
});

jQuery(document).on('click', '.add-favourite', function(){
    var id = jQuery(this).attr('object-id');

    addFavourites(id);
    jQuery('.box_'+id+' button[name=add-to-favourites]').removeClass('add-favourite').addClass('remove-favourite');
    jQuery('.box_'+id+' button[name=add-to-favourites]').html('remove from favourites');
});

jQuery(document).on('click', '.remove-favourite', function(){
    var id = jQuery(this).attr('object-id');
    console.log("remove");
    removeFavourite(id);
    jQuery('.box_'+id+' button[name=add-to-favourites]').removeClass('remove-favourite').addClass('add-favourite');
    jQuery('.box_'+id+' button[name=add-to-favourites]').html('add to favourites');
});

jQuery(document).on('click', '.trailer_btn', function(){
    var trigger = jQuery(this).attr('target-id');
    jQuery('[trigger-id='+trigger+']').slideToggle();
});

if(typeof(localStorage.fav)  !== 'undefined' && localStorage.fav !== 'undefined'){
    var fav =  JSON.parse(localStorage.fav);
    populate(fav, '#favourites');
}

if(typeof(localStorage.shows)  !== 'undefined' && localStorage.shows !== 'undefined'){
    var last = JSON.parse(localStorage.shows);
    populate(last, '#results');
}

getPopular();


$("img").error(function () {
    $(this).unbind("error").attr("src", "res/offline_small.png");
});

window.applicationCache.onupdateready = function(e){
    if(window.applicationCache.status === window.applicationCache.UPDATEREADY){
        if(confirm('New version available. Want to reload?')){
            window.location.reload();
        }
    }
};

jQuery(document).on('change', '[type=checkbox]', function() {
    var id = jQuery(this).attr('name');
    if (jQuery(this).is(':checked')) {
        console.log("Save episode");
        if(typeof(localStorage) !== 'undefined'){
            if(typeof(localStorage.ep) !== 'undefined' && localStorage.ep !== 'undefined'){
                var store = JSON.parse(localStorage.ep) || [];
                store.push(id);
                localStorage.ep = JSON.stringify(store);
            } else {
                var arr = [];
                arr.push(id);
                localStorage.ep = JSON.stringify(arr);
            }
        }
    } else {
        var episodes = JSON.parse(localStorage.ep);
        var index = episodes.indexOf(id);

        if (index > -1) {
            console.log(id);
            episodes.splice(index, 1);
        }
        localStorage.ep = JSON.stringify(episodes);
        console.log("Forget episode");
    }
});

function mergeArrays(arr1, arr2){
    var i,p,obj={},result=[];
    for(i=0;i<arr1.length;i++){
        obj[arr1[i].id]=arr1[i]
    }
    for(i=0;i<arr2.length;i++){
        obj[arr2[i].id]=arr2[i]
    }
    for(p in obj){
        if(obj.hasOwnProperty(p)){
            result.push({id:p,value:obj[p]})
        }
    }
    return result;
}